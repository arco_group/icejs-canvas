# -*- mode: makefile-gmake; coding: utf-8 -*-

PROXY = "CanvasRegistry%20-t:ws%20-h%20127.0.0.1%20-p%2010001"

all:

run: run-iceserver run-webserver

run-iceserver:
	python server.py

run-webserver: botcanvas.js
	@echo "visit 'http://127.0.0.1:8000/html5-svg-canvas.html?p=$(PROXY)\n'"
	python -m SimpleHTTPServer

botcanvas.js: botcanvas.ice
	slice2js -I $(shell python -c "import Ice; print Ice.getSliceDir()") $^


.PHONY: clean
clean:
	$(RM) botcanvas.js *~
