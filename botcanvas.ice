// -*- mode: c++; coding: utf-8 -*-

#include <Ice/Identity.ice>

module Robots {

    struct BotInfo {
	string name;
	string color;
	int x;
	int y;
    };

    sequence<BotInfo> BotSeq;

    struct MissileInfo {
	string id;
	string color;
	short angle;
	int x;
	int y;
    };

    sequence<MissileInfo> MissileSeq;

    interface Canvas {
	void draw(BotSeq bots, MissileSeq missiles);
    };

    interface CanvasRegistry {
	void attach(Ice::Identity oid);
    };
};
