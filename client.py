#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
import random
import math
import time
import Ice

Ice.loadSlice("botcanvas.ice -I {} --all".format(Ice.getSliceDir()))
import Robots


class Robot(object):
    MAX_SPEED = 5

    def __init__(self, name, color):
        self.info = Robots.BotInfo(
            name = name,
            color = color,
            x = random.randint(1, 999),
            y = random.randint(1, 999))

        self.angle = 0
        self.speed = 0

    def drive(self, angle, speed):
        self.angle = angle
        self.speed = speed

    def update(self):
        r = (self.MAX_SPEED * self.speed) / 100.0
        dx = round(r * math.cos(self.angle))
        dy = round(r * math.sin(self.angle))
        self.info.x += dx
        self.info.y += dy


class Client(Ice.Application):
    def run(self, args):
        if len(args) != 2:
            print "Usage: {} <proxy>".format(args[0])
            return 1

        ic = self.communicator()
        canvas = ic.stringToProxy(args[1])
        canvas = Robots.CanvasPrx.uncheckedCast(canvas)

        self.play(canvas)

    def play(self, canvas):
        self.b1 = Robot("BOT1", "#F44336")
        self.b2 = Robot("BOT2", "#2196F3")
        self.b3 = Robot("BOT3", "#CDDC39")
        self.b4 = Robot("BOT4", "#607D8B")

        self.bots = [self.b1.info, self.b2.info,
                     self.b3.info, self.b4.info]
        self.missiles = []

        self.b1.drive(45, 100)

        for i in range(10):
            self.update_bots()
            self.update_missiles()
            canvas.draw(self.bots, self.missiles)
            time.sleep(0.2)

    def update_bots(self):
        self.b1.update()

    #     bot1 = self.bots[0]
    #     bot2 = self.bots[1]

    #     bot1.x += 5
    #     if abs(bot1.x - bot2.x) < 5:
    #         m = Robots.MissileInfo(
    #             id=bot1.name, color="black", angle=0, x=bot1.x, y=bot1.y)
    #         self.missiles.append(m)

    #     return self.bots

    def update_missiles(self):
        pass

    #     for m in self.missiles[:]:
    #         m.y -= 10
    #         if abs(m.y - self.bots[1].y) < 5:
    #             self.missiles.remove(m)

    #     return self.missiles


if __name__ == '__main__':
    Client().main(sys.argv)
