// -*- mode: js; coding: utf-8 -*-

CanvasI = Ice.Class(Robots.Canvas, {
    __init__: function() {
	this.ctx = null;
	this.bot_names = [];

	// NOTE: when this method is called, document.readyState will
	// be 'complete', as is in an asyn call

	var element = $('#myCanvas').get(0);
	if (! element || ! element.getContext)
            return;

	this.ctx = element.getContext('2d');
	if (! this.ctx || ! this.ctx.drawImage)
            return;

	// common context setup
	this.ctx.font = '20px FontAwesome';
    },

    draw: function(bots, missiles, current) {
	if (! this.ctx) {
	    console.error("No drawing context ready, nothing done.");
	    return;
	}

	this.clear();
	this.render_bots(bots);
	this.render_missiles(missiles);
    },

    update_legend: function(bot) {
	var legend = $('#myLegend');

	this.bot_names.push(bot.name);
	var item = '<div><i class="fa fa-fw fa-stop-circle-o" style="color: '+
	    bot.color + '"></i>&nbsp;&nbsp;' + bot.name + '</div>';
	legend.append(item);
    },

    render_bots: function(bots) {
	for (var i in bots) {
	    var bot = bots[i];

	    // render bot
	    this.ctx.fillStyle = bot.color;
	    this.ctx.fillText("\uf28e", bot.x, bot.y);

	    // update legend
	    if (this.bot_names.indexOf(bot.name) == -1)
		this.update_legend(bot);
	}
    },

    render_missiles: function(missiles) {
	for (var i in missiles) {
	    var missile = missiles[i];

	    this.ctx.fillStyle = missile.color;

	    // this.ctx.save();
	    // this.ctx.rotate(-45 * Math.PI / 180);
	    this.ctx.fillText("\uf135", missile.x, missile.y);
	    // this.ctx.restore();
	}
    },

    clear: function() {
	this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    },
});

var ic;

Ice.Promise.try(function() {
    ic = Ice.initialize();

    // get proxy from url
    var param = window.location.search.substring(1).split("&")[0];
    var proxy = decodeURI(param.split("=")[1]);

    // create proxy to server
    var registry_proxy = ic.stringToProxy(proxy);

    // create local object adapter
    return ic.createObjectAdapter("").then(on_adapter_ready);

    var canvas_proxy;
    var adapter;

    // add servant to object adapter
    function on_adapter_ready(adapter_) {
	adapter = adapter_;
    	canvas_proxy = adapter.addWithUUID(new CanvasI());

	return Robots.CanvasRegistryPrx.checkedCast(registry_proxy)
	    .then(on_registry_ready);
    };

    // register servant's proxy on canvas registry
    function on_registry_ready(registry) {
	connection = registry.ice_getCachedConnection();
	connection.setAdapter(adapter);

	return registry.attach(canvas_proxy.ice_getIdentity());
    };

}).exception(function(ex) {
    console.log(ex.toString());

    if (ic)
	return ic.destroy();
});
