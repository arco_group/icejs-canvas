#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
from threading import RLock
import Ice

Ice.loadSlice("botcanvas.ice -I {} --all".format(Ice.getSliceDir()))
import Robots


class CanvasRegistryI(Robots.CanvasRegistry):
    def __init__(self):
        self.clients = []
        self.lock = RLock()

    def attach(self, oid, current):
        print "- attach:", oid.name

        client = current.con.createProxy(oid).ice_oneway()
        client = Robots.CanvasPrx.uncheckedCast(client)

        with self.lock:
            self.clients.append(client)

    def detach(self, client):
        with self.lock:
            self.clients.remove(client)


class Forwarder(Robots.Canvas):
    def __init__(self, registry):
        self.clients = registry.clients
        self.registry = registry

    def draw(self, bots, missiles, current):
        for c in self.clients[:]:
            print "- draw:", c.ice_getIdentity().name

            try:
                c.draw(bots, missiles)
            except Ice.Exception:
                print "ERROR: discarding client ({})".format(c.ice_getIdentity().name)
                self.registry.detach(c)

        return True, buffer("")


class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("Adapter")
        adapter.activate()

        registry = CanvasRegistryI()
        forwarder = Forwarder(registry)

        adapter.add(registry, ic.stringToIdentity("CanvasRegistry"))
        canvas_prx = adapter.add(forwarder, ic.stringToIdentity("Canvas"))

        print "Canvas: '{}'".format(canvas_prx)
        print "Waiting events..."

        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == '__main__':
    Server().main(sys.argv, "server.config")
