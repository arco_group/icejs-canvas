// -*- mode: js; coding: utf-8 -*-

var WIDTH = 1000;
var HEIGHT = 1000;

Robot = function(ctx, bot) {
    this.ctx = ctx;
    this.r = this.ctx.text("\uf28e");
    this.r.font({size: 20, family: "FontAwesome", fill: bot.color});

    this.update(bot);
};

Robot.prototype.update = function(bot) {
    // source origin is bottom-left corner, transform 'y' for top-left corner
    this.r.move(bot.x, HEIGHT - bot.y);
};

Missile = function(ctx, missile) {
    this.ctx = ctx;

    this.r = this.ctx.group();
    this.rocket = this.r.text("\uf135");
    this.rocket.font({size: 20, family: "FontAwesome", fill: missile.color});

    this.update(missile);
};

Missile.prototype.update = function(missile) {
    // source origin is bottom-left corner, transform 'y' for top-left corner
    this.r.move(missile.x, HEIGHT - missile.y);

    // source angles are CCW, SVG is CW, transform it
    this.rocket.rotate(- missile.angle - 45);
};

CanvasI = Ice.Class(Robots.Canvas, {
    __init__: function() {
	this.bots = {};
	this.missiles = {};

	this.ctx = SVG("mySVG").size(WIDTH, HEIGHT);
    },

    draw: function(bots, missiles, current) {
	if (! this.ctx) {
	    console.error("No drawing context ready, nothing done.");
	    return;
	}

	this.update_bots(bots);
	this.update_missiles(missiles);
    },

    update_legend: function(bot) {
	var legend = $('#myLegend');

	var item = '<div><i class="fa fa-fw fa-stop-circle-o" style="color: '+
	    bot.color + '"></i>&nbsp;&nbsp;' + bot.name + '</div>';
	legend.append(item);
    },

    update_bots: function(bots) {
	for (var i in bots) {
	    var bot = bots[i];

	    if (this.bots[bot.name] === undefined) {
		this.bots[bot.name] = new Robot(this.ctx, bot);
		this.update_legend(bot);
		continue;
	    }

	    this.bots[bot.name].update(bot);
	}
    },

    update_missiles: function(missiles) {
	for (var i in missiles) {
	    var missile = missiles[i];

	    if (this.missiles[missile.id] === undefined) {
		this.missiles[missile.id] = new Missile(this.ctx, missile);
		continue;
	    }

	    this.missiles[missile.id].update(missile);
	}
    },
});

var ic;
var connection;

Ice.Promise.try(function() {
    var idata = new Ice.InitializationData();
    idata.properties = Ice.createProperties();

    // idata.properties.setProperty("Ice.Default.Locator", "something");

    ic = Ice.initialize(idata);

    // get proxy from url
    var param = window.location.search.substring(1).split("&")[0];
    var proxy = decodeURI(param.split("=")[1]);

    // create proxy to server
    var registry_proxy = ic.stringToProxy(proxy);

    // create local object adapter
    return ic.createObjectAdapter("").then(on_adapter_ready);

    var canvas_proxy;
    var adapter;

    // add servant to object adapter
    function on_adapter_ready(adapter_) {
	adapter = adapter_;
    	canvas_proxy = adapter.addWithUUID(new CanvasI());

	return Robots.CanvasRegistryPrx.checkedCast(registry_proxy)
	    .then(on_registry_ready);
    };

    // register servant's proxy on canvas registry
    function on_registry_ready(registry) {
	connection = registry.ice_getCachedConnection();
	connection.setAdapter(adapter);

	return registry.attach(canvas_proxy.ice_getIdentity());
    };

}).exception(function(ex) {
    console.log(ex.toString());

    if (ic)
	return ic.destroy();
});
